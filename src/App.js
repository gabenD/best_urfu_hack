import React from 'react';
import axios from 'axios';
import DirList from './components/dirlist';
import Line from './components/line';
import './App.css';

const ID = "fa600505c29e4e469c3f01fbbf3bc5de";
const NAME = "token_name";

let urll;
urll = {
  req: undefined,
  get_token_url: () => {
    try {
      let token = /access_token=([^&]+)/.exec(document.location.hash)[1];
      if (token) {
        localStorage.setItem(NAME, token);
        urll.page_update();
      }
    } catch (e) {
      return urll.get_token_local();
    }
  },

  get_token_local: () => {
    let token = localStorage.getItem(NAME)
    if (token) {
      return token;
    }
    else {
      urll.get_token_yandex();
    }
  },

  get_token_yandex: () => {
    let url = urll.create_link_token();
    document.location.href = url;
  },

  create_link_token: () => {
    let auth = "https://oauth.yandex.ru/authorize";
    let type = "response_type=token";
    let client_id = "client_id=" + ID;
    return auth + "?" + type + "&" + client_id;
  },
  get_url_yandex_path: (path) => {
    let url = "https://cloud-api.yandex.net:443/v1/disk/resources?path=" + path + "&preview_crop=true&sort=name";
    return url;
  },
  page_update: () => {
    document.location.href = "/";
  },
  start: () => {
    return urll.get_token_url();
  },
  clear: () => {
    localStorage.clear(NAME);
  },


}
class App extends React.Component{
  flag = true;
  state = {
    folder: undefined,
    item: undefined
  }
  getDirectory = async () => {
  let token = urll.start();
  console.log("token get complete!");
  let url = urll.get_url_yandex_path(window.location.pathname);
  axios.defaults.headers.common['Authorization'] = 'OAuth ' + token;
  let resp = await axios.get(url);
  if (resp['error'] === undefined) {
    this.setState({folder: resp['data']['name'], item:resp['data']['_embedded']['items']})
  }
  else{
    urll.page_update();
  }
  }

  render() {
    if (this.flag){
      this.getDirectory();
      this.flag = false;
    }
    // name, size, modify, path, type
    return(
        <section className="main">
          <DirList items={this.state.item}></DirList>
        </section>
    )
  }
}

export default App;
