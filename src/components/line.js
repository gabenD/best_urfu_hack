import React from 'react';
function str_to_time(string) {
    if (string == 'Дата изм.'){
        return string;
    }
    let data = new Date(string);
    return data.toLocaleDateString();

}
function size_to_string(string) {
    if (string == 'Размер'){
        return string;
    }
    if (string) {
        return (string / 8 / 1024 / 1024).toFixed(3) + ' Mb';
    }
    return '';
}
function path_normal(path) {
    return path.replace('disk:', '');
}
class Line extends React.Component{
    render() {
        // name, size, modified, path, type
        return (
            <div>
                {this.props.name &&
                <div className="line">
                    <div className="line-ch icon"></div>
                    <div className="line-ch name">
                        {this.props.type !== 'dir' ? (
                            this.props.name
                        ):(
                            <a href={path_normal(this.props.path)}>{this.props.name}</a>
                        )}

                    </div>
                    <div className="line-ch modified">
                        {str_to_time(this.props.modified)}
                    </div>
                    <div className="line-ch size">{size_to_string(this.props.size)}</div>
                </div>
                }
            </div>
        )
    }
}

export default Line;